dir="$PWD"
version="0.0.17"

brancName=""
product="com.jago.digitalBankingApp"
env="dev"
red=$(tput setaf 1)
none=$(tput sgr0)
device=""

cleardata=false
moveBranch=false
pullLatest=false

show_help() {
printf "
usage:
    -p, --path
        path to the project
    -cl, --clear-data
        clear app data before running flutter
    --env
        change environment used when uninstalling
    -h, --help
        print this message
    -pl, --pull-latest
        pull latest branch before running
    -b, --branch
        change current branch
    --build-apk
        build android apk file
    -t, --test
        testing project
    -tc, --test-coverage
        testing project with coverage
    -d, --device
        select device for testing
    setup
        preparing this file to use globally
    update
        update current flrun
    -v, --version
        print current installed version
"
exit 0
}

run_setup() {
    rc=""
    case $SHELL in
    "/bin/zsh")
        rc="$HOME/.zshrc"
        ;;
    "/bin/bash")
        rc="$HOME/.bash_profile"
        ;;
    esac

    echo $rc

    case $rc in
    "")
        echo "terminal not supported"
        ;;
    *)
        echo "Installing..."
        cp "$dir/run.sh" "$HOME/.run-flutter.sh"
        chmod +x "$HOME/.run-flutter.sh"
        if grep -q 'alias flrun=~/.run-flutter.sh' "$rc"; then
        echo "flrun already added to rc"
        else
        sh -c "echo 'alias flrun=~/.run-flutter.sh' >> $rc"
        fi
        echo "Complete :D"
        echo "Please reopen Terminal"
        echo "Open help menu with 'flrun -h'"
        ;;
    esac

    exit 0
}

run_update() {
    echo "Updating flrun..."
    if [ -d "flrun" ]; then
        git -C flrun/ pull
    else
        git clone git@gitlab.com:herisetiawan_/flrun.git
    fi
    cd flrun
    git pull
    chmod +x $dir/flrun/run.sh
    $dir/flrun/run.sh setup
    echo "Clearing self"
    rm -rf $dir/flrun
    echo "Updating complete :D"
    exit 0
}

branch_setup() {
    git fetch
    if [ $moveBranch == true ]; then
        git checkout $brancName
    fi
    git pull
}

set_env() {
    env=$1
}

set_path() {
    dir=$1
}

clear_data() {
    case $device in
    "" | "all")
    adb devices | while read line
    do
        if [ ! "$line" = "" ] && [ `echo $line | awk '{print $2}'` = "device" ]
        then
        dev=`echo $line | awk '{print $1}'`
        echo "$dev..."
        adb -s $dev uninstall ${product}_${env} || echo "$device clean"
        fi
    done
    ;;
    *)
    adb -s $device uninstall ${product}_${env} || echo "$device clean"
    ;;
    esac

}

build_apk() {
    flutter build apk
    open $dir/build/app/outputs/apk/release/
    exit 0
}

run_test() {
    flutter test $1
    exit 0
}

run_tests() {
    if [ ! -z "$1" ]; then
      dir=$1
    fi
    if [[ -f "pubspec.yaml" ]]; then
        rm -f coverage/lcov.info
        rm -f coverage/lcov-final.info
        flutter test --no-test-assets --coverage $dir
    else
        printf "\n${red}Error: this is not a Flutter project${none}"
        exit 1
    fi
}

run_report() {
    if [[ -f "coverage/lcov.info" ]]; then
        lcov -r coverage/lcov.info '*/__test*__/*' -o coverage/lcov_cleaned.info
        genhtml -o coverage coverage/lcov_cleaned.info
        open coverage/index-sort-l.html
    else
        printf "\n${red}Error: no coverage info was generated${none}"
        exit 1
    fi
}

do_action() {
    case $1 in
    -h | --help)
        show_help
        ;;
    -p | --path)
        set_path $2
        ;;
    -cl | --clear-data)
        cleardata=true
        ;;
    --env)
        set_env $2
        ;;
    -pl | --pull-latest)
        pullLatest=true
        ;;
    -b | --branch)
        moveBranch=true
        brancName=$2
        ;;
    --build-apk)
        build_apk
        ;;
    -t | --test)
        run_test $2
        ;;
    -tc | --test-coverage)
        run_tests $2
        run_report
        exit 0
        ;;
    -d | --device)
        device="$2"
        ;;
    setup)
        run_setup
        ;;
    update)
        run_update
        ;;
    -v | --version)
        echo $version
        exit 0
        ;;
    
    esac
}



do_action $1 $2
do_action $2 $3
do_action $3 $4
do_action $4 $5
do_action $5 $6
do_action $6 $7
do_action $7 $8
do_action $8 $9

echo "ENV: $env"
echo "PATH: $dir"
echo "PACKAGE NAME: ${product}_$env"
echo "DIRECTORY: $dir"
cd $dir

if [ $pullLatest == true ] || [ $moveBranch == true ]; then
    echo "Pulling latest branch..."
    branch_setup
fi

if [ $cleardata == true ]; then
    echo "Clearing all data..."
    clear_data
fi


case $device in
"")
    flutter run
    ;;
*)
    flutter run -d $device
    ;;
esac